/**
 * description: generate the table with fields to book an appointement
 */
let httpRequest;
if (window.XMLHttpRequest) {
    httpRequest = new XMLHttpRequest()
} else {
    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
httpRequest.onreadystatechange = function () {
    if (this.readyState === 4 && this.status == 200) {

        let tableEl = document.getElementsByTagName('table');
        /**
         * @description remove table if it is already exists
         */
        if (tableEl[0] !== undefined) {
            tableEl[0].remove()
        }
        let body = document.getElementsByTagName('body')[0];
        let table = document.createElement('table');
        table.setAttribute("id", "tab01");

        /**
         * @description : slots available for patients
         */
        let tbody = document.createElement('tbody');
        let thead = document.createElement('thead');
        let headTr = document.createElement('tr');


        /**
         * @description creating form element to book an appointment
         */
        let headTd1 = document.createElement('td');
        const headTd1text = document.createTextNode("Id");
        headTd1.appendChild(headTd1text);

        let headTd2 = document.createElement('td');
        const headTd2text = document.createTextNode("Email ID:");
        headTd2.appendChild(headTd2text);

        let headTd3 = document.createElement('td');
        const headTd3text = document.createTextNode("Start time");
        headTd3.appendChild(headTd3text);

        let headTd4 = document.createElement('td');
        const headTd4text = document.createTextNode("End time");
        headTd4.appendChild(headTd4text);

        let headTd5 = document.createElement('td');
        const headTd5text = document.createTextNode("Specialization");
        headTd5.appendChild(headTd5text);


        let headTd6 = document.createElement('td');
        const headTd6text = document.createTextNode("Action");
        headTd6.appendChild(headTd6text);

        headTr.appendChild(headTd1);
        headTr.appendChild(headTd2);
        headTr.appendChild(headTd3);
        headTr.appendChild(headTd4);
        headTr.appendChild(headTd5);
        headTr.appendChild(headTd6);

        thead.appendChild(headTr);

        let data = JSON.parse(this.response);
        let len = data.length;

        if (len > 0) {
            for (let i = 0; i < len; i++) {
                let tbodyTr = document.createElement('tr');

                let td1 = document.createElement('td');
                const td1Text = document.createTextNode(data[i].id);
                td1.appendChild(td1Text);

                let td2 = document.createElement('td');
                const td2Text = document.createTextNode(data[i].demail);
                td2.appendChild(td2Text);

                let td3 = document.createElement('td');
                const td3Text = document.createTextNode(data[i].starttime);
                td3.appendChild(td3Text);

                let td4 = document.createElement('td');
                const td4Text = document.createTextNode(data[i].endtime);
                td4.appendChild(td4Text);

                let td5 = document.createElement('td');
                const td5Text = document.createTextNode(data[i].specialize);
                td5.appendChild(td5Text);

                /**
                 * @description booking an appointment
                 */
                let td6 = document.createElement('td');
                let bt1 = document.createElement('button');
                bt1.setAttribute("class", "ubutton");
                let bt1txt = document.createTextNode("Booking Slot");
                bt1.addEventListener("click", function () {
                    let useremail = JSON.parse(JSON.stringify(sessionStorage.getItem("email")));
                    let data = this.parentElement.parentElement.cells;
                    console.log("data: " + data[0].innerHTML + " " + data[1].innerHTML + "  " + data[2].innerHTML + " " + data[3].innerHTML + " " + data[4].innerHTML);
                    let demail = data[1].innerHTML;
                    let starttime = data[2].innerHTML;
                    let endtime = data[3].innerHTML;
                    let specialize = data[4].innerHTML;
                    let email = useremail;
                    let obj = { demail, starttime, endtime, specialize, email };
                    console.log("Object: " + obj);

                    /**
                     * @description Ajax service invocation to booking an appointment
                     */
                    let httpReq;
                    if (window.XMLHttpRequest) {
                        httpReq = new XMLHttpRequest();
                    }
                    else {
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    httpReq.onreadystatechange = function () {
                        if (this.readyState === 4 && this.status === 201) {
                            console.log(`response:  ${this.response}`);
                            alert("Your Appointment placed Successfully! ");
                        }
                    }
                    const url = 'http://localhost:3000/appointment';
                    httpReq.open('POST', url, true);
                    httpReq.setRequestHeader("Content-type", "application/json");
                    httpReq.send(JSON.stringify(obj));
                })

                bt1.appendChild(bt1txt);
                td6.appendChild(bt1);
                tbodyTr.appendChild(td1);
                tbodyTr.appendChild(td2);
                tbodyTr.appendChild(td3);
                tbodyTr.appendChild(td4);
                tbodyTr.appendChild(td5);
                tbodyTr.appendChild(td6);
                tbody.appendChild(tbodyTr);
            }
        }
        /**
         * @description display message if no record found
         */
        else {
            const data = document.createElement("h4");
            const noData = document.createTextNode("No data Found")
            data.appendChild(noData);
            tbody.appendChild(data);
        }
        table.appendChild(thead);
        table.appendChild(tbody);
        body.appendChild(table);
    }
}
const url = 'http://localhost:3000/slots';
httpRequest.open('GET', url, true);
httpRequest.send();
