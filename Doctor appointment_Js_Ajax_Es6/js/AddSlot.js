/****
 * @author Prasanna Danda
 * Add slot for Application
 * 
 */

/**
 * @description Doctor will add the Available slot details 
 */

let updateHeading = document.createElement("h2");
updateHeading.appendChild(document.createTextNode("Add Doctor Details for Available slots"));
updateHeading.setAttribute("align", 'center');

/**
* @description : save the data when doctor add slot
*/
let doctorMailId = document.createElement("input");
doctorMailId.setAttribute("type", "text");
doctorMailId.setAttribute("id", 'demail');
doctorMailId.setAttribute("placeholder", 'Enter doctor name');
doctorMailId.setAttribute("required", "true");

let specialize = document.createElement("select");

let opt1 = document.createElement('option');
opt1.appendChild(document.createTextNode('General physician'));
opt1.value = 'General physician';
specialize.appendChild(opt1);

let opt2 = document.createElement('option');
opt2.appendChild(document.createTextNode('Dermatoloist'));
opt2.value = 'Dermatoloist';
specialize.appendChild(opt2);

let opt3 = document.createElement('option');
opt3.appendChild(document.createTextNode('Homoepath'));
opt3.value = 'Homoepath';
specialize.appendChild(opt3);

let opt4 = document.createElement('option');
opt4.appendChild(document.createTextNode('Ayurvada'));
opt4.value = 'Ayurvada';
specialize.appendChild(opt4);

specialize.setAttribute("id", 'specialize');
specialize.setAttribute("required", 'true');

let starttime = document.createElement("input");
starttime.setAttribute("type", "datetime-local");
starttime.setAttribute("id", 'starttime');
starttime.setAttribute("placeholder", 'Start time');
starttime.setAttribute("required", "true");

let endtime = document.createElement("input");
endtime.setAttribute("type", "datetime-local");
endtime.setAttribute("id", 'endtime');
endtime.setAttribute("placeholder", 'Enter end time');
endtime.setAttribute("required", "true");

let linebreak = document.createElement("br");

let doctormailDiv = document.createElement("div");
doctormailDiv.appendChild(document.createTextNode("Doctor Email Id"));

let specializeDiv = document.createElement("div");
specializeDiv.appendChild(document.createTextNode("Specialization:"));

let startTimeDiv = document.createElement("div");
startTimeDiv.appendChild(document.createTextNode("Start Time:"));

let endTimeDiv = document.createElement("div");
endTimeDiv.appendChild(document.createTextNode("End Time:"));

let emptyDiv = document.createElement("div");

let body = document.getElementsByTagName('body')[0];

document.body.appendChild(updateHeading);

document.body.appendChild(doctormailDiv);
body.appendChild(doctorMailId)

document.body.appendChild(specializeDiv);
body.appendChild(specialize);

document.body.appendChild(startTimeDiv);
body.appendChild(starttime);

document.body.appendChild(endTimeDiv);
body.appendChild(endtime);


body.appendChild(emptyDiv);
body.appendChild(linebreak);

/**
 * @description which used to create action button to add slot details
 * 
 */

let addProductDbbtn = document.createElement("button");
let addProductDbTextNode = document.createTextNode("Add Slot");
addProductDbbtn.appendChild(addProductDbTextNode);
addProductDbbtn.addEventListener('click', function () {
    addSlot();
});

body.appendChild(addProductDbbtn);

/**
  * @description Function to set adding slot details
  * 
  */
const addSlot = () => {
    /**
     * @description Form fields declaration
     * 
     */
    const email = document.getElementById("demail").value;
    const specialize = document.getElementById("specialize").value;
    const starttime = document.getElementById("starttime").value;
    const endtime = document.getElementById("endtime").value;

    /**
     * @description Form fields validations
     * 
     */
    if (!email.length > 0) {
        alert("Please Enter emailId");
        return false;
    }
    if (!specialize.length > 0) {
        alert("Please Enter Specialization");
        return false;
    } else if (!starttime.length > 0) {
        alert("Please Enter starttime");
        return false;
    } else if (!endtime.length > 0) {
        alert("Please Enter endTime");
        return false;
    }

    /**
     * @description Form fields short hand properties
     * 
     */

    const email = demail;
    const obj = { email, specialize, starttime, endtime };

    /**
     * @description Ajax service invocation to add slot details
     * 
     */
    let httpReq;
    if (window.XMLHttpRequest) {
        httpReq = new XMLHttpRequest();
    } else {
        httpReq = ActiveXObject("Microsoft.XMLHTTP");
    }
    httpReq.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 201) {
            alert("Slot successfully Added... !!");
            console.log(this.response);
        }
    }
    const url = 'http://localhost:3000/slots';
    httpReq.open('POST', url, true);
    httpReq.setRequestHeader("Content-type", "application/json");
    httpReq.send(JSON.stringify(obj));
}


