/****
 * @author Danda Prasanna
 * login script for Application
 * 
 */
const getData = () => {

    /**
      * @description Form fields declaration
      */
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    const login = document.getElementById("role").value;

    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const pattern = "[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)";

    /**
     * @description Form fields validations
     */
    if (email === "" || !email.match(mailformat) || !email.match(pattern)) {
        alert("Please Enter Valid Mail Address with patterns hcl or dbs");
        email.focus();
        return false;
    }
    else if (password === "") {
        alert("Please Enter Valid Password");
        password.focus();
        return false;
    }
    else if (login === "-1") {
        alert("Please Enter Valid Role");
        role.focus();
        return false;
    }
    const xhttp = new XMLHttpRequest();
    /**
      * @description Ajax service invocation for booking slot and appointment      
      */
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            console.log(this.responseText);
            localStorage.setItem("login", login);
            if (login === "doctor") {
                window.location.href = "addslot.html"
            } else {
                window.location.href = "appointmentbooking.html"
            }
        }
    };
    // var url = "http://localhost:3000/" + login + "?email=" + email + "&password=" + password;

    /**
     * @description Ajax call using literals
     */
    const url = `http://localhost:3000/${login}?email=${email}&password=${password} `;
    xhttp.open('get', url, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}

/**
 * @description This method fetches all the user details
 * @param {*} response 
 */

const displayTableData = (response) => {
    let body = document.getElementsByTagName('body')[0];
    let table = document.createElement('table');

    /**
      * @description delete table if it is already exists
      * 
      */

    let removeTable = document.getElementsByTagName('table');
    if (removeTable[0] !== undefined) {
        removeTable[0].remove()
    }

    /*table.setAttribute("border", "1");*/
    table.setAttribute("id", "users");


    let tbody = document.createElement('tbody');
    let thead = document.createElement('thead');
    let headTr = document.createElement('tr');

    let headTd1 = document.createElement('td');
    const headTd1text = document.createTextNode("id");
    headTd1.appendChild(headTd1text);

    let headTd2 = document.createElement('td');
    const headTd2text = document.createTextNode("username");
    headTd2.appendChild(headTd2text);

    let headTd3 = document.createElement('td');
    const headTd3text = document.createTextNode("address");
    headTd3.appendChild(headTd3text);

    let headTd4 = document.createElement('td');
    const headTd4text = document.createTextNode("email");
    headTd4.appendChild(headTd4text);

    let headTd5 = document.createElement('td');
    const headTd5text = document.createTextNode("password");
    headTd5.appendChild(headTd5text);

    let headTd6 = document.createElement('td');
    const headTd6text = document.createTextNode("mobileNumber");
    headTd6.appendChild(headTd6text);

    let headTd7 = document.createElement('td');
    const headTd7text = document.createTextNode("Action");
    headTd7.appendChild(headTd7text);

    headTr.appendChild(headTd1);
    headTr.appendChild(headTd2);
    headTr.appendChild(headTd3);
    headTr.appendChild(headTd4);
    headTr.appendChild(headTd5);
    headTr.appendChild(headTd6);
    headTr.appendChild(headTd7);

    thead.appendChild(headTr);
    let data = JSON.parse(response);
    let len = data.length;
    /**
     * @description store loggedin user data
     */
    if (len > 0) {
        for (let i = 0; i < len; i++) {
            let tbodyTr = document.createElement('tr');

            let td1 = document.createElement('td');
            const td1Text = document.createTextNode(data[i].id);
            td1.appendChild(td1Text);

            let td2 = document.createElement('td');
            const td2Text = document.createTextNode(data[i].username);
            td2.appendChild(td2Text);

            let td3 = document.createElement('td');
            const td3Text = document.createTextNode(data[i].address);
            td3.appendChild(td3Text);

            let td4 = document.createElement('td');
            const td4Text = document.createTextNode(data[i].email);
            td4.appendChild(td4Text);

            let td5 = document.createElement('td');
            const td5Text = document.createTextNode(data[i].password);
            td5.appendChild(td5Text);

            let td6 = document.createElement('td');
            const td6Text = document.createTextNode(data[i].mobileNumber);
            td6.appendChild(td6Text);

            let td7 = document.createElement('td');
            let productBtn = document.createElement("button");
            let productBtnTxt = document.createTextNode("Add Product");
            productBtn.addEventListener('click', function () {
                let data = this.parentElement.parentElement.cells;
                console.log(data);
                let id = data[0].innerHTML;
                let username = data[1].innerHTML;
                let address = data[2].innerHTML;
                let email = data[3].innerHTML;
                let password = data[4].innerHTML;
                let mobileNumber = data[5].innerHTML;
                /**
                 * @description short hand properties
                 */
                let obj1 = { id, username, address, email, password, mobileNumber };
                console.log("Object: " + obj1);
                localStorage.setItem("userId", id);
                window.location.assign("product.html");                                                
            })

            productBtn.appendChild(productBtnTxt);
            td7.appendChild(productBtn);
            tbodyTr.appendChild(td1);
            tbodyTr.appendChild(td2);
            tbodyTr.appendChild(td3);
            tbodyTr.appendChild(td4);
            tbodyTr.appendChild(td5);
            tbodyTr.appendChild(td6);
            tbodyTr.appendChild(td7);
            tbody.appendChild(tbodyTr);
        }
    }
    /**
     * description: display message if no record found
     */
    else {
        const data = document.createElement("h4");
        const noData = document.createTextNode("No data Found")
        data.appendChild(noData);
        tbody.appendChild(data);
    }

    table.appendChild(thead);
    table.appendChild(tbody);
    body.appendChild(table);
}
