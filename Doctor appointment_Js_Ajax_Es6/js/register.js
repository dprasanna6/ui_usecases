/**
 * @author Danda Prasanna
 * @description register user script for an application
 */

const validateRegister = () => {
    /**
     * @description form fields declaration
     * 
     */
    const userPassword = document.getElementById("password");
    userPassword.value = Array(10).fill("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz").map(function (x) { return x[Math.floor(Math.random() * x.length)] }).join('');
    const name = document.getElementById("username").value;
    const mobile = document.getElementById("mobileNumber").value;
    const email = document.getElementById("email").value;
    const dob = document.getElementById("dob").value;
    userPassword = document.getElementById("password").value;

    const username = name;
    const password = userPassword;

    /**
     * @description short hand properties
     * 
     */

    const obj = { username, mobileNumber, email, dob, password };
    console.log(obj);

    /**
     * @description form fields validations
     * 
     */

    if (name === "") {
        alert("Please Enter name");
        email.focus();
        return false;
    }
    else if (mobile === "") {
        alert("Please Enter Valid mobile");
        password.focus();
        return false;
    }
    else if (email === "") {
        alert("Please Enter Valid email with dbs or hcl");
        role.focus();
        return false;
    }
    else if (dob === "") {
        alert("Please Enter Valid dob");
        role.focus();
        return false;
    }
    else if (userPassword === "") {
        alert("Please Enter Valid userpassword");
        role.focus();
        return false;
    }

    let xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }

    return new Promise(function (resolve, reject) {
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4) { //(this.readyState == 4 && this.status == 201)
                if (this.status !== 201) {
                    reject(`User Registration Failed! Status code: ${this.status}`);
                }
                else {
                    console.log(`response:  ${this.response}`);
                    resolve("User Successfully registered")
                }
            }
        }

        /**
         * @description Ajax call invocation to register patient
         * 
         */
        const url = 'http://localhost:3000/patient';
        xmlhttp.open('post', url, true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.send(JSON.stringify(obj));
    });
    promise.then((response) => {
        console.log(`response:  ${this.response}`);
        window.location.href = "login.html";
    }).catch((error) => {
        console.log(error);
    })
}
