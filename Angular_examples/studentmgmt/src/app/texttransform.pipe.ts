import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'texttransform'
})
export class TexttransformPipe implements PipeTransform {

  // transform(value: unknown, ...args: unknown[]): unknown {
  //   return null;
  // }


  transform(students:any[]): any {  
    return students.filter(studentId => studentId.equals("id").map("id"+"Dna"));
  }
}
