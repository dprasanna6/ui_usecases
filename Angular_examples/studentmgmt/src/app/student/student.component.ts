import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  students : Array<any> =[{
    id: "DNA",
    firstName: 'Prasanna',
    lastName: 'Danda',
    studentId: 1,
    address: 'hyderabad',
    mobile: 8464082533,
    emailId:'dprasanna2509@gmail.com'
  },
  {
    id: "DNA",
    firstName: 'ram',
    lastName: 'ddd',
    studentId: 2,
    address: 'warangal',
    mobile: 9160508552,
    emailId:'prashanthi@gmail.com'
  }
  
  ]
   selectedStudentData : any=null;
 
  constructor() { }
   trackByStudentId =(index :number ,student:any) =>
   {
     return student.id;
  }
 

  onSelect =(student) =>{
    this.selectedStudentData=student;
    console.log("In Parent",student);   
  }

  // onClearSelect =(student) =>{
  //   let index = this.students.indexOf(student);
  //   this.students.splice(index, 1);
  //   console.log("In clear Parent",student);   
  // }

  ngOnInit(): void {
  }

}
