import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentComponent } from './student/student.component';
import { UsersComponent } from './users/users.component';
import { TexttransformPipe } from './texttransform.pipe';

@NgModule({
  declarations: [
    AppComponent,  
    StudentComponent,
    UsersComponent,
    TexttransformPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
