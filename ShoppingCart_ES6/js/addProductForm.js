/**
 * @description displaying list of products in the form of table
 */

let updateHeading = document.createElement("h2");
updateHeading.appendChild(document.createTextNode("Product Information"));
updateHeading.setAttribute("align", 'center');

/**
 * @description creating javascript elements
 */

let productId = document.createElement("input");
productId.setAttribute("type", "number");
productId.setAttribute("id", 'productId');
productId.setAttribute("placeholder", 'Enter Product Id');
productId.setAttribute("required", "true");

/**
 * @description productname element
 * 
 */
let productName = document.createElement("input");
productName.setAttribute("type", "text");
productName.setAttribute("id", 'name');
productName.setAttribute("pattern", '[A-Za-z]+');
productName.setAttribute("autofocus", "on");
productName.setAttribute("placeholder", 'Enter Product Name');
productName.setAttribute("required", "true");

/**
 * @description javascript form elements
 * 
 */
let desc = document.createElement("input");
desc.setAttribute("type", "text");
desc.setAttribute("id", 'description');
desc.setAttribute("placeholder", 'Enter Product Description');
desc.setAttribute("required", "true");

let productPrice = document.createElement("input");
productPrice.setAttribute("type", "number");
productPrice.setAttribute("id", 'price');
productPrice.setAttribute("placeholder", 'Enter Product Price');
productPrice.setAttribute("required", 'true');

/**
 * @description rating element
 * 
*/

let rating = document.createElement("select");

let opt1 = document.createElement('option');
opt1.appendChild(document.createTextNode('excellent'));
opt1.value = 'excellent';
rating.appendChild(opt1);

let opt2 = document.createElement('option');
opt2.appendChild(document.createTextNode('good'));
opt2.value = 'good';
rating.appendChild(opt2);

let opt3 = document.createElement('option');
opt3.appendChild(document.createTextNode('average'));
opt3.value = 'good';

rating.appendChild(opt3);
let opt4 = document.createElement('option');
opt4.appendChild(document.createTextNode('poor'));
opt4.value = 'good';
rating.appendChild(opt4);

rating.setAttribute("id", 'rating');
rating.setAttribute("required", 'true');
let linebreak = document.createElement("br");

/**
 * @description add product button 
*/

let addProductDbbtn = document.createElement("button");
let addProductDbTextNode = document.createTextNode("Add Product");
addProductDbbtn.appendChild(addProductDbTextNode);
addProductDbbtn.addEventListener('click', function () {
  addProduct();
});

/**
 * @description display product button 
*/
let displayProdbtn = document.createElement("button");
let displayProdbtnTextNode = document.createTextNode("Show Products");
displayProdbtn.appendChild(displayProdbtnTextNode);
displayProdbtn.addEventListener('click', function () {
  displayProducts();
});

let prodIdDiv = document.createElement("div");
prodIdDiv.appendChild(document.createTextNode("Product ID:"));

let nameDiv = document.createElement("div");
nameDiv.appendChild(document.createTextNode("Product Name:"));

let descDiv = document.createElement("div");
descDiv.appendChild(document.createTextNode("Product Description:"));

let priceDiv = document.createElement("div");
priceDiv.appendChild(document.createTextNode("Price:"));

let ratingDiv = document.createElement("div");
ratingDiv.appendChild(document.createTextNode("Rating:"));

let emptyDiv = document.createElement("div");

// let emptyDiv = document.createElement("div");

let body = document.getElementsByTagName('body')[0];

document.body.appendChild(updateHeading);
document.body.appendChild(prodIdDiv);
body.appendChild(productId)

document.body.appendChild(nameDiv);
body.appendChild(productName);

document.body.appendChild(descDiv);
body.appendChild(desc);
document.body.appendChild(priceDiv);
body.appendChild(productPrice);

document.body.appendChild(ratingDiv);
body.appendChild(rating);
body.appendChild(emptyDiv);
body.appendChild(linebreak);
body.appendChild(addProductDbbtn);
body.appendChild(displayProdbtn);

/**
 * @description Adding product details
 * 
 */

const addProduct = () => {
  const productId = document.getElementById("productId").value;
  const name = document.getElementById("name").value;
  const description = document.getElementById("description").value;
  const price = document.getElementById("price").value;
  const rating = document.getElementById("rating").value;

  if (!productId.length > 0) {
    alert("Please Enter productId");
    return;
  }
  if (!name.length > 0) {
    alert("Please Enter Name");
    return;
  } else if (!description.length > 0) {
    alert("Please Enter description");
    return;
  } else if (!price.length > 0) {
    alert("Please Enter price");
    return;
  } else if (!rating.length > 0) {
    alert("Please Enter rating");
    return;
  }

  /**
   * @description shorthand properties
   */

  let obj = { productId, name, description, price, rating };
  let httpReq;
  if (window.XMLHttpRequest) {
    httpReq = new XMLHttpRequest();
  } else {
    httpReq = ActiveXObject("Microsoft.XMLHTTP");
  }

  /**
   * @description applied promise feature for ajax call
   * 
   */
  return new Promise(function (resolve, reject) {
    httpReq.onreadystatechange = function () {
      if (this.readyState === 4) { //here actual condition if (this.readyState === 4 && this.status === 201)
        if (this.status != 201) {
          reject(`Error code  ${this.status}`);
        } else {
          alert("Product Successfuly Added... !!");
          console.log(this.response);
        }
      }
    }

    const url = 'http://localhost:3000/products';
    httpReq.open('POST', url, true);
    httpReq.setRequestHeader("Content-type", "application/json");
    httpReq.send(JSON.stringify(obj));
  });
  promise.then((response) => {
    console.log(`response:  ${this.response}`);
  }).catch((error) => {
    console.log(error);
  })
}

/**
 * 
 * @description displaying product details
 */
const displayProducts = () => {
  let tableEl = document.getElementsByTagName('table');
  if (tableEl[0] !== undefined) {
    tableEl[0].remove()
  }

  let xmlhttp;
  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  }
  xmlhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      console.log(this.response);
      displayTableData(this.response);
    }
  }
  const url = 'http://localhost:3000/products';
  xmlhttp.open('GET', url, true);
  xmlhttp.setRequestHeader("Content-type", "application/json");
  xmlhttp.send(null);
  // });
}

/**
 * @description This method fetches all the user details
 * @param {*} response 
 */

const displayTableData = (response) => {
  let body = document.getElementsByTagName('body')[0];
  let table = document.createElement('table');

  /**
   * @description Delete the table if already exists
   * 
   */

  let removeTable = document.getElementsByTagName('table');
  if (removeTable[0] !== undefined) {
    removeTable[0].remove()
  }


  table.setAttribute("id", "users");


  let tbody = document.createElement('tbody');
  let thead = document.createElement('thead');
  let headTr = document.createElement('tr');

  let headTd1 = document.createElement('td');
  const headTd1text = document.createTextNode("Product Id");
  headTd1.appendChild(headTd1text);

  let headTd2 = document.createElement('td');
  const headTd2text = document.createTextNode("Name");
  headTd2.appendChild(headTd2text);

  let headTd3 = document.createElement('td');
  const headTd3text = document.createTextNode("Description");
  headTd3.appendChild(headTd3text);

  let headTd4 = document.createElement('td');
  const headTd4text = document.createTextNode("Price");
  headTd4.appendChild(headTd4text);

  let headTd5 = document.createElement('td');
  const headTd5text = document.createTextNode("Rating");
  headTd5.appendChild(headTd5text);

  let headTd6 = document.createElement('td');
  const headTd6text = document.createTextNode("Action");
  headTd6.appendChild(headTd6text);


  // headTd7.appendChild(cartBtn);
  headTr.appendChild(headTd1);
  headTr.appendChild(headTd2);
  headTr.appendChild(headTd3);
  headTr.appendChild(headTd4);
  headTr.appendChild(headTd5);
  headTr.appendChild(headTd6);
  //headTr.appendChild(td7);


  thead.appendChild(headTr);
  let data = JSON.parse(response);
  let len = data.length;

  if (len > 0) {
    for (let i = 0; i < len; i++) {
      let tbodyTr = document.createElement('tr');

      let td1 = document.createElement('td');
      const td1Text = document.createTextNode(data[i].productId);
      td1.appendChild(td1Text);

      let td2 = document.createElement('td');
      const td2Text = document.createTextNode(data[i].name);
      td2.appendChild(td2Text);

      let td3 = document.createElement('td');
      const td3Text = document.createTextNode(data[i].description);
      td3.appendChild(td3Text);

      let td4 = document.createElement('td');
      const td4Text = document.createTextNode(data[i].price);
      td4.appendChild(td4Text);

      let td5 = document.createElement('td');
      const td5Text = document.createTextNode(data[i].rating);
      td5.appendChild(td5Text);

      let td6 = document.createElement('td');

      let updatebutton = document.createElement('button');
      let updatebuttontxt = document.createTextNode("Add To Cart");
      updatebutton.addEventListener("click", function () {
        let data = this.parentElement.parentElement.cells;
        //  console.log("data: " + data[0].innerHTML + " " + data[1].innerHTML + "  " + data[2].innerHTML + " " + data[3].innerHTML + " " + data[4].innerHTML);

        let userId = localStorage.getItem("id");
        let productId = data[0].innerHTML;
        let name = data[1].innerHTML;
        let description = data[2].innerHTML;
        let price = data[3].innerHTML;
        let rating = data[4].innerHTML;

        let obj = { productId, name, description, price, rating };

        localStorage.setItem("products", JSON.stringify(obj));
        console.log("Successfully added item to cart")
        window.location.href = "cartDetails.html";
      })

      updatebutton.appendChild(updatebuttontxt);
      td6.appendChild(updatebutton);

      tbodyTr.appendChild(td1);
      tbodyTr.appendChild(td2);
      tbodyTr.appendChild(td3);
      tbodyTr.appendChild(td4);
      tbodyTr.appendChild(td5);
      tbodyTr.appendChild(td6);
      tbody.appendChild(tbodyTr);
    }
  }

  /**
   * @description If table data not present
   * 
   */
  else {
    const data = document.createElement("h4");
    const noData = document.createTextNode("No data Found")
    data.appendChild(noData);
    tbody.appendChild(data);
  }

  table.appendChild(thead);
  table.appendChild(tbody);

  body.appendChild(table);
}

