/**
 * @description place order item
 */

const placeOrderItem = () => {
  let updateHeading = document.createElement("h2");
  updateHeading.appendChild(document.createTextNode("Place Order"));
  updateHeading.setAttribute("align", 'center');

  let userId = document.createElement("input");
  userId.setAttribute("type", "number");
  userId.setAttribute("id", 'userId');
  userId.setAttribute("placeholder", 'Enter User Id');
  userId.setAttribute("required", "true");


  let productId = document.createElement("input");
  productId.setAttribute("type", "text");
  productId.setAttribute("id", 'productId');
  productId.setAttribute("placeholder", 'Enter Product Id');
  productId.setAttribute("required", "true");


  let orderDate = document.createElement("input");
  orderDate.setAttribute("type", "Date");
  orderDate.setAttribute("id", 'orderDate');
  orderDate.setAttribute("placeholder", 'Enter Date');
  orderDate.setAttribute("required", "true");


  let productName = document.createElement("input");
  productName.setAttribute("type", "text");
  productName.setAttribute("id", 'name');
  productName.setAttribute("placeholder", 'Enter Product Name');
  productName.setAttribute("required", "true");


  let linebreak = document.createElement("br");

  let addOrderDbbtn = document.createElement("button");
  let addOrderDbTextNode = document.createTextNode("Place Order");
  addOrderDbbtn.appendChild(addOrderDbTextNode);
  addOrderDbbtn.addEventListener('click', function () {
    addOrder();
  });

  let displayorderbtn = document.createElement("button");
  let displayorderbtnTextNode = document.createTextNode("Show Order");
  displayorderbtn.appendChild(displayorderbtnTextNode);
  displayorderbtn.addEventListener('click', function () {
    displayOrders();
  });

  let userIdDiv = document.createElement("div");
  userIdDiv.appendChild(document.createTextNode("User ID:"));

  let prodIdDiv = document.createElement("div");
  prodIdDiv.appendChild(document.createTextNode("Product ID:"));

  let nameDiv = document.createElement("div");
  nameDiv.appendChild(document.createTextNode("Product Name:"));

  let orderDateDiv = document.createElement("div");
  orderDateDiv.appendChild(document.createTextNode("Ordered Date:"));

  let emptyDiv = document.createElement("div");

  let body = document.getElementsByTagName('body')[0];

  document.body.appendChild(updateHeading);

  document.body.appendChild(userIdDiv);
  body.appendChild(userId);

  document.body.appendChild(prodIdDiv);
  body.appendChild(productId);

  document.body.appendChild(orderDateDiv);
  body.appendChild(orderDate);

  document.body.appendChild(nameDiv);
  body.appendChild(productName);

  body.appendChild(emptyDiv);
  body.appendChild(linebreak);
  body.appendChild(addOrderDbbtn);
  body.appendChild(displayorderbtn);
}

/**
 * @description this method used to place the order
 * 
 */

const addOrder=() =>{
  const userId = document.getElementById("userId").value;
  const productId = document.getElementById("productId").value;
  const orderDate = document.getElementById("orderDate").value;
  const name = document.getElementById("name").value;


  if (!userId.length > 0) {
    alert("Please Enter userId");
    return;
  }
  if (!productId.length > 0) {
    alert("Please Enter Product Id");
    return;
  } else if (!orderDate.length > 0) {
    alert("Please Enter order Date");
    return;
  } else if (!name.length > 0) {
    alert("Please Enter productName");
    return;
  }
  let cartlist = JSON.parse(localStorage.getItem("cartlist"));
  console.log("cartlist :=>" + cartlist.length);
  let today = new Date();
  let date = `${today.getFullYear()} - ${today.getMonth() + 1} - ${today.getDate()}`;
  let time = `${today.getHours()} : ${today.getMinutes()}  :  ${today.getSeconds()}`; 
  let dateTime = `${date} ${time}`;
  let orderObj;
  for (i in cartlist) {
    orderObj = {
      productId: cartlist[i].productId,
      name: cartlist[i].name,
      userId: cartlist[i].userId,
      price: cartlist[i].price,
      orderDate: dateTime.toString()
    };
  }

  let httpReq;
  if (window.XMLHttpRequest) {
    httpReq = new XMLHttpRequest();
  } else {
    httpReq = ActiveXObject("");
  }
  httpReq.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 201) {
      console.log(this.response);
      alert("Ordered placed Successfuly ... !!");     
    }
  }
  const url = 'http://localhost:3000/orders';
  httpReq.open('post', url, true);
  httpReq.setRequestHeader("Content-type", "application/json");
  httpReq.send(JSON.stringify(orderObj));
}

/**
 * @description get the list of orders
 * 
*/

const displayOrders=() => {
  let tableEl = document.getElementsByTagName('table');
  if (tableEl[0] !== undefined) {
    tableEl[0].remove()
  }

  let xmlhttp;
  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  }
  xmlhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      alert(this.response);
      displayTableData(this.response);
    }
  }

  const userId=localStorage.getItem("userId");
  const url=`http://localhost:3000/orders?userId= ${userId}`;
  xmlhttp.open('get', url, true);
  xmlhttp.setRequestHeader("Content-type", "application/json");
  xmlhttp.send(null);  
}

/**
 * @description This method fetches all the order details
 */

const displayTableData = (response) =>{
  let body = document.getElementsByTagName('body')[0];
  let table = document.createElement('table');

  /**
   * @description Delete the table if already exists
   * 
   */
  
  let removeTable = document.getElementsByTagName('table');
  if (removeTable[0] !== undefined) {
    removeTable[0].remove()
  }

   table.setAttribute("id", "orders");

   
  /**
   * @description displaying product form
   * 
   */
  let tbody = document.createElement('tbody');
  let thead = document.createElement('thead');
  let headTr = document.createElement('tr');

  let headTd1 = document.createElement('td');
  const headTd1text = document.createTextNode("User Id");
  headTd1.appendChild(headTd1text);

  let headTd2 = document.createElement('td');
  const headTd2text = document.createTextNode("Product Id");
  headTd2.appendChild(headTd2text);

  let headTd3 = document.createElement('td');
  const headTd3text = document.createTextNode("Order Date");
  headTd3.appendChild(headTd3text);

  let headTd4 = document.createElement('td');
  const headTd4text = document.createTextNode("Product Name");
  headTd4.appendChild(headTd4text);

  headTr.appendChild(headTd1);
  headTr.appendChild(headTd2);
  headTr.appendChild(headTd3);
  headTr.appendChild(headTd4);


  thead.appendChild(headTr);
  let data = JSON.parse(response);
  let len = data.length;

  if (len > 0) {
    for (var i = 0; i < len; i++) {
      let tbodyTr = document.createElement('tr');

      let td1 = document.createElement('td');
      const td1Text = document.createTextNode(data[i].userId);
      td1.appendChild(td1Text);

      let td2 = document.createElement('td');
      const td2Text = document.createTextNode(data[i].productId);
      td2.appendChild(td2Text);

      let td3 = document.createElement('td');
      const td3Text = document.createTextNode(data[i].orderDate);
      td3.appendChild(td3Text);

      let td4 = document.createElement('td');
      const td4Text = document.createTextNode(data[i].name);
      td4.appendChild(td4Text);

      tbodyTr.appendChild(td1);
      tbodyTr.appendChild(td2);
      tbodyTr.appendChild(td3);
      tbodyTr.appendChild(td4);
      tbody.appendChild(tbodyTr);
    }
  }
  /**
   * @description if table data not present
   * 
  */   
  else {
    const data = document.createElement("h4");
    const noData = document.createTextNode("No data Found")
    data.appendChild(noData);
    tbody.appendChild(data);
  }

  table.appendChild(thead);
  table.appendChild(tbody);
  body.appendChild(table);
}


