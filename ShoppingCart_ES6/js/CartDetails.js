/**
 * @description addCart Details
 * 
 */

const addCart = () => {
  const productId = document.getElementById("productId").value;
  const name = document.getElementById("name").value;
  const price = document.getElementById("price").value;
  const userId = document.getElementById("userId").value;
  const qty = document.getElementById("prodQty").value;
  const productPrice = parseInt(price) * parseInt(qty);
  const productpricedetails = productPrice + "";

  /**
   * @description checking javascript validations 
   * 
   */

  if (!productId.length > 0) {
    alert("Please Enter productId");
    return;
  }
  else if (!name.length > 0) {
    alert("Please Enter Name");
    return;
  } else if (!price.length > 0) {
    alert("Please Enter price");
    return;
  }
  else if (!userId.length > 0) {
    alert("Please Enter userId");
    return;
  }
  else if (!qty.length > 0) {
    alert("Please Enter qty");
    return;
  }

  /**
   * @description creating object for all elements
   * 
   */
  let price = productpricedetails;
  let prodQty = qty;
  let obj = { productId, name, userId, price, prodQty };
  console.log(obj);
  let httpReq;
  if (window.XMLHttpRequest) {
    httpReq = new XMLHttpRequest();
  } else {
    httpReq = ActiveXObject("Microsoft.XMLHTTP");
  }
  httpReq.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 201) {
      alert("Item Added to cart successfully... !!");
      console.log(this.response);
      //getData();
    }
  }
  const url = 'http://localhost:3000/carts';
  httpReq.open('post', url, true);
  httpReq.setRequestHeader("Content-type", "application/json");
  httpReq.send(JSON.stringify(obj));
}

/**
 * @description displaying cart product details
 * 
 */

const getCartProduct = () => {
  const data = JSON.parse(localStorage.getItem("products"));
  const cartUserId = localStorage.getItem("userId");
  console.log("data: " + data.productName);
  document.getElementById("productId").value = data.productId;
  document.getElementById("name").value = data.name;
  document.getElementById("userId").value = cartUserId;
  document.getElementById("price").value = data.price;
}

/**
 * @description getcart details
 * 
 */

const getCarts = () => {
  let tableEl = document.getElementsByTagName('table');
  if (tableEl[0] !== undefined) {
    tableEl[0].remove()
  }

  let httpReq;
  if (window.XMLHttpRequest) {
    httpReq = new XMLHttpRequest();
  }
  else {
    httpReq = new ActiveXObject("")
  }
  httpReq.onreadystatechange = function () {

    if (this.readyState === 4 && this.status === 200) {
      console.log(this.response);
      localStorage.setItem("carts", this.response);
      displaytableCart(this.response);
    }
  }
  const userId = localStorage.getItem("userId")
  const url = `http://localhost:3000/carts?userId= ${userId}`;
  httpReq.open('get', url, true);
  httpReq.send();
}

/**
 * @description get the list of cart items
 * @param {*} response 
 */

function displaytableCart(response) {

  let body = document.getElementsByTagName('body')[0];
  let table = document.createElement('table');
  table.setAttribute("border", "1");


  let tbody = document.createElement('tbody');
  let thead = document.createElement('thead');
  let headTr = document.createElement('tr');

  let headTd1 = document.createElement('td');
  const headTd1text = document.createTextNode("Id");
  headTd1.appendChild(headTd1text);

  let headTd2 = document.createElement('td');
  const headTd2text = document.createTextNode("productId");
  headTd2.appendChild(headTd2text);

  let headTd3 = document.createElement('td');
  const headTd3text = document.createTextNode("name");
  headTd3.appendChild(headTd3text);

  let headTd4 = document.createElement('td');
  const headTd4text = document.createTextNode("userId");
  headTd4.appendChild(headTd4text);

  let headTd5 = document.createElement('td');
  const headTd5text = document.createTextNode("price");
  headTd5.appendChild(headTd5text);

  let headTd6 = document.createElement('td');
  const headTd6text = document.createTextNode("prodQty");
  headTd6.appendChild(headTd6text);

  headTr.appendChild(headTd1);
  headTr.appendChild(headTd2);
  headTr.appendChild(headTd3);
  headTr.appendChild(headTd4);
  headTr.appendChild(headTd5);
  headTr.appendChild(headTd6);

  thead.appendChild(headTr);
  let data = JSON.parse(response);
  console.log(data);
  let len = data.length;

  if (len > 0) {
    for (var i = 0; i < len; i++) {
      let tbodyTr = document.createElement('tr');

      let td1 = document.createElement('td');
      const td1Text = document.createTextNode(data[i].id);
      td1.appendChild(td1Text);

      let td2 = document.createElement('td');

      const td2Text = document.createTextNode(data[i].productId);
      td2.appendChild(td2Text);

      let td3 = document.createElement('td');
      const td3Text = document.createTextNode(data[i].name);
      td3.appendChild(td3Text);

      let td4 = document.createElement('td');
      const td4Text = document.createTextNode(data[i].userId);
      td4.appendChild(td4Text);

      let td5 = document.createElement('td');
      const td5Text = document.createTextNode(data[i].price);
      td5.appendChild(td5Text);

      let td6 = document.createElement('td');
      const td6Text = document.createTextNode(data[i].prodQty);
      td6.appendChild(td6Text);
      tbodyTr.appendChild(td1);
      tbodyTr.appendChild(td2);
      tbodyTr.appendChild(td3);
      tbodyTr.appendChild(td4);
      tbodyTr.appendChild(td5);

      tbodyTr.appendChild(td6);
      tbody.appendChild(tbodyTr);
    }
  }
  else {
    const data = document.createElement("h4");
    const noData = document.createTextNode("No data Found")
    data.appendChild(noData);
    tbody.appendChild(data);
  }

  table.appendChild(thead);
  table.appendChild(tbody);
  body.appendChild(table);

}

const addNewProductsToCart = () => {
  window.location.href = "Product.html";
}

/**
 * @description Used to remove cart items
 * 
 */

const removeCart = () => {
  var xmlhttp;
  if (window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
  }
  xmlhttp.onreadystatechange = function () {
    if (this.status === 200 && this.readyState === 4) {
      alert("removed cart items Successfully");
    }
  }
  const url = 'http://localhost:3000/carts';
  xmlhttp.open('DELETE', url, true);
  xmlhttp.setRequestHeader('Content-type', 'application/json');
  xmlhttp.send(null);
}


/**
 * @description function place order data
 * 
 */

const placeOrderData = () => {
  let cartlist = JSON.parse(localStorage.getItem("carts"));
  console.log("cartlist :=>" + cartlist.length);
  let today = new Date();
  // let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
  let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  //  let dateTime = date + ' ' + time; 
  let dateTime = `${date} ${time}`;
  let date = `${today.getFullYear()} - ${today.getMonth() + 1} - ${today.getDate()}`;
  for (i in cartlist) {
    const { productId, name, userId, price } = cartlist[i];
    let orderObj = {
      productId,
      name,
      userId,
      price,
      orderDate: dateTime.toString()
    };

    placeOrder(orderObj);
  }
}