/**
 * @description get the login details from Db 
 */
function getData() {
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;

    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const pattern = "[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)";

    /**
     * @description email and password validations
     * 
    */    
    if (email == "" || !email.match(mailformat) || !email.match(pattern)) {
      alert("Please Enter Valid Mail Address with patterns hcl or dbs");
      email.focus();
      return false;
    }
    else if (password == "") {
      alert("Please Enter Valid Password");
      password.focus();
      return false;
    }

    let tableEl = document.getElementsByTagName('table');
    if (tableEl[0] !== undefined) {
      tableEl[0].remove()
    }
    loginForm.addEventListener('submit', (event) => {
      event.preventDefault();
      let email = document.getElementById("email").value;
      let password = document.getElementById("password").value;

      let xmlhttp;
      if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
      }
      let xhttp = new XMLHttpRequest();

      /**
       * @description ajax call for login data
       * 
       */
      
      xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          console.log(this.responseText);
          alert("login successfully");
          displayTableData(this.response)
        }
      };

      //var url = 'http://localhost:3000/users?email=' + email + '&password=' + password;
      let url =`http://localhost:3000/users?email=${email}&password=${password}`;     
      alert(url);  
      xhttp.open('GET', url, true);
      xhttp.setRequestHeader("Content-type", "application/json");
      xhttp.send();

    });
  }

  /**
   * This method fetches all the user details
   * @param {*} response 
   */  
  function displayTableData(response) {
    let body = document.getElementsByTagName('body')[0];
    let table = document.createElement('table');

    /**
     * @description Delete the table if already exists
     * 
     */    
    const removeTable = document.getElementsByTagName('table');
    if (removeTable[0] !== undefined) {
      removeTable[0].remove()
    }

    table.setAttribute("id", "users");

     /**
      * @description displaying data in the from of table
      * 
      */     
    let tbody = document.createElement('tbody');
    let thead = document.createElement('thead');
    let headTr = document.createElement('tr');

    let headTd1 = document.createElement('td');
    const headTd1text = document.createTextNode("Id");
    headTd1.appendChild(headTd1text);

    let headTd2 = document.createElement('td');
    const headTd2text = document.createTextNode("Username");
    headTd2.appendChild(headTd2text);

    let headTd3 = document.createElement('td');
    const headTd3text = document.createTextNode("Address");
    headTd3.appendChild(headTd3text);

    let headTd4 = document.createElement('td');
    const headTd4text = document.createTextNode("Email");
    headTd4.appendChild(headTd4text);

    let headTd6 = document.createElement('td');
    const headTd6text = document.createTextNode("Mobile Number");
    headTd6.appendChild(headTd6text);

    let headTd7 = document.createElement('td');
    const headTd7text = document.createTextNode("Action");
    headTd7.appendChild(headTd7text);

    headTr.appendChild(headTd1);
    headTr.appendChild(headTd2);
    headTr.appendChild(headTd3);
    headTr.appendChild(headTd4);  
    headTr.appendChild(headTd6);
    headTr.appendChild(headTd7);

    thead.appendChild(headTr);
    let data = JSON.parse(response);
    let len = data.length;

    if (len > 0) {
      for (let i = 0; i < len; i++) {
        let tbodyTr = document.createElement('tr');

        let td1 = document.createElement('td');
        const td1Text = document.createTextNode(data[i].id);
        td1.appendChild(td1Text);

        let td2 = document.createElement('td');
        const td2Text = document.createTextNode(data[i].username);
        td2.appendChild(td2Text);

        let td3 = document.createElement('td');
        const td3Text = document.createTextNode(data[i].address);
        td3.appendChild(td3Text);

        let td4 = document.createElement('td');
        const td4Text = document.createTextNode(data[i].email);
        td4.appendChild(td4Text);

        let td6 = document.createElement('td');
        const td6Text = document.createTextNode(data[i].mobileNumber);
        td6.appendChild(td6Text);

        let td7 = document.createElement('td');
        let productBtn = document.createElement("button");
        let productBtnTxt = document.createTextNode("Add Product");
        productBtn.addEventListener('click', function () {
          let data = this.parentElement.parentElement.cells;
          console.log(data);
          let obj1 = { id: data[0].innerHTML, username: data[1].innerHTML, address: data[2].innerHTML, email: data[3].innerHTML, password: data[4].innerHTML, mobileNumber: data[5].innerHTML };
          console.log("Object: " + obj1);
          localStorage.setItem("userId", data[0].innerHTML);
          window.location.assign("product.html");                                           
        })

        productBtn.appendChild(productBtnTxt);
        td7.appendChild(productBtn);
        tbodyTr.appendChild(td1);
        tbodyTr.appendChild(td2);
        tbodyTr.appendChild(td3);
        tbodyTr.appendChild(td4);      
        tbodyTr.appendChild(td6);
        tbodyTr.appendChild(td7);
        tbody.appendChild(tbodyTr);
      }
    }

    /**
     * @description displaying message if table data not found
     * 
    */
     else {
      const data = document.createElement("h4");
      const noData = document.createTextNode("No data Found")
      data.appendChild(noData);
      tbody.appendChild(data);
    }

    table.appendChild(thead);
    table.appendChild(tbody);
    body.appendChild(table);
  }
