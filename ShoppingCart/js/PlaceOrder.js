function placeOrderItem(){
var updateHeading =  document.createElement("h2");
updateHeading.appendChild(document.createTextNode("Place Order"));
updateHeading.setAttribute("align",'center');

var userId = document.createElement("input");
userId.setAttribute("type","number");
userId.setAttribute("id",'userId');
userId.setAttribute("placeholder",'Enter User Id');
userId.setAttribute("required","true");


var productId = document.createElement("input");
productId.setAttribute("type","text");
productId.setAttribute("id",'productId');
productId.setAttribute("placeholder",'Enter Product Id');
productId.setAttribute("required","true");


var orderDate = document.createElement("input");
orderDate.setAttribute("type","Date");
orderDate.setAttribute("id",'orderDate');
orderDate.setAttribute("placeholder",'Enter Date');
orderDate.setAttribute("required","true");


var productName = document.createElement("input");
productName.setAttribute("type","text");
productName.setAttribute("id",'name');
productName.setAttribute("placeholder",'Enter Product Name');
productName.setAttribute("required","true");


var linebreak = document.createElement("br");

var addOrderDbbtn = document.createElement("button");
var addOrderDbTextNode = document.createTextNode("Place Order");
addOrderDbbtn.appendChild(addOrderDbTextNode);
addOrderDbbtn.addEventListener('click',function()
{
   addOrder();
});

var displayorderbtn = document.createElement("button");
var displayorderbtnTextNode = document.createTextNode("Show Order");
displayorderbtn.appendChild(displayorderbtnTextNode);
displayorderbtn.addEventListener('click',function()
{
   displayOrders();
});

var userIdDiv=  document.createElement("div");
userIdDiv.appendChild(document.createTextNode("User ID:"));

var prodIdDiv=  document.createElement("div");
prodIdDiv.appendChild(document.createTextNode("Product ID:"));

var nameDiv=  document.createElement("div");
nameDiv.appendChild(document.createTextNode("Product Name:"));

var orderDateDiv=  document.createElement("div");
orderDateDiv.appendChild(document.createTextNode("Ordered Date:"));

var emptyDiv=  document.createElement("div");

var emptyDiv=  document.createElement("div");

var body = document.getElementsByTagName('body')[0];

document.body.appendChild(updateHeading);

document.body.appendChild(userIdDiv);
body.appendChild(userId);

document.body.appendChild(prodIdDiv);
body.appendChild(productId);

document.body.appendChild(orderDateDiv);
body.appendChild(orderDate);

document.body.appendChild(nameDiv);
body.appendChild(productName);

body.appendChild(emptyDiv);
body.appendChild(linebreak);
body.appendChild(addOrderDbbtn);
body.appendChild(displayorderbtn);
}

function addOrder(){
    var userId= document.getElementById("userId").value;
   var productId= document.getElementById("productId").value;
   var orderDate= document.getElementById("orderDate").value;
   var name=document.getElementById("name").value;
   
   
       if(!userId.length>0){
         alert("Please Enter userId");
          return;
        }
       if(!productId.length>0){
            alert("Please Enter Product Id");
            return;
       }else if(!orderDate.length>0){
             alert("Please Enter order Date");    
             return;       
        }else if(!name.length>0){
               alert("Please Enter productName");   
               return;        
        }
       var cartlist = JSON.parse(localStorage.getItem("cartlist"));
        console.log("cartlist :=>"+cartlist.length);
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
         alert(dateTime);
         var orderObj;
          for (i in cartlist) {
              
                 orderObj = {
                   productId : cartlist[i].productId, 
                   name : cartlist[i].name , 
                   userId :cartlist[i].userId,
                   price:cartlist[i].price,
                   orderDate : dateTime.toString()
               };
            }  
  
   var httpReq;
   if(window.XMLHttpRequest){
       httpReq=new XMLHttpRequest();
   }else{
       httpReq=ActiveXObject("");
   }
   httpReq.onreadystatechange=function(){
       if(this.readyState===4 && this.status===200){
       alert("Ordered placed Successfuly ... !!");
       console.log(this.response);
     //getData();
       }
   }
   httpReq.open("post","http://localhost:3000/orders",true);
   httpReq.setRequestHeader("Content-type","application/json");
   httpReq.send(JSON.stringify(orderObj));
}


function displayOrders(){  
   var tableEl = document.getElementsByTagName('table');
     if (tableEl[0] !== undefined) {
         tableEl[0].remove()
     } 
           
  var xmlhttp;
   if(window.XMLHttpRequest){
      xmlhttp=new XMLHttpRequest(); 
   }            
   xmlhttp.onreadystatechange = function() {   
   if (this.readyState === 4 && this.status === 200) {      
     console.log(this.response);
     displayTableData(this.response);
    }   
    }

    
    xmlhttp.open('get', 'http://localhost:3000/orders?userId='+localStorage.getItem("userId") , true);
    xmlhttp.setRequestHeader("Content-type","application/json");
    xmlhttp.send(null);     
   // });
 } 
 
 //This method fetches all the user details
  function displayTableData(response)
  {
   var body = document.getElementsByTagName('body')[0];
   var table = document.createElement('table');
   //Delete the table if already exists
 
    var removeTable = document.getElementsByTagName('table');
     if (removeTable[0] !== undefined) {
         removeTable[0].remove()
      }
                
    /*table.setAttribute("border", "1");*/
    table.setAttribute("id", "orders");
    
    
     var tbody = document.createElement('tbody');
     var thead = document.createElement('thead');
     var headTr = document.createElement('tr');
     
     var headTd1 = document.createElement('td');
     var headTd1text = document.createTextNode ("userId");
     headTd1.appendChild(headTd1text);
     
     var headTd2 = document.createElement('td');
     var headTd2text = document.createTextNode ("productId");
     headTd2.appendChild(headTd2text);
     
     var headTd3 = document.createElement('td');
     var headTd3text = document.createTextNode("orderDate");
     headTd3.appendChild(headTd3text);
 
     var headTd4 = document.createElement('td');
     var headTd4text = document.createTextNode ("productName");
     headTd4.appendChild(headTd4text);
     
     headTr.appendChild(headTd1);
     headTr.appendChild(headTd2);
     headTr.appendChild(headTd3);
     headTr.appendChild(headTd4);
            
     
     thead.appendChild(headTr);
     var data = JSON.parse(response);
     var len = data.length;
     
     if(len > 0 ){
     for(var i=0; i<len;i++){
         var tbodyTr = document.createElement('tr');
 
         var td1 = document.createElement('td');
         var td1Text = document.createTextNode(data[i].userId);
         td1.appendChild(td1Text);
 
         var td2 = document.createElement('td');
         var td2Text = document.createTextNode(data[i].productId);
         td2.appendChild(td2Text);
 
         var td3 = document.createElement('td');
         var td3Text = document.createTextNode(data[i].orderDate);
         td3.appendChild(td3Text);
 
         var td4 = document.createElement('td');
         var td4Text = document.createTextNode(data[i].name);
         td4.appendChild(td4Text);
 
          
         var td6=document.createElement('td');

         tbodyTr.appendChild(td1);
         tbodyTr.appendChild(td2);
         tbodyTr.appendChild(td3);
         tbodyTr.appendChild(td4);            
         tbodyTr.appendChild(td6);           
         tbody.appendChild(tbodyTr); 
     }
     }
     else{
         var data = document.createElement("h4");
         var noData = document.createTextNode("No data Found")
         data.appendChild(noData);
         tbody.appendChild(data);
     }
     
     table.appendChild(thead);
     table.appendChild(tbody);     
     body.appendChild(table);
   }


//  function getOrderStorageData(){
//  var cartlist = JSON.parse(localStorage.getItem("carts"));
//  console.log("cartlist :=>"+cartlist.length);
//  var today = new Date();
//   var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
//   var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
//   var dateTime = date+' '+time;
//   alert(dateTime);
//    for (i in cartlist) {
       
//         var orderObj = {
//             productId : cartlist[i].productId, 
//             name : cartlist[i].name , 
//             userId :cartlist[i].userId,
//             price:cartlist[i].price,
//             orderDate : dateTime.toString()
//         };
        
//       placeOrder(orderObj);
//     }// window.location.href="PlaceOrder.html";
//   }