var updateHeading =  document.createElement("h2");
updateHeading.appendChild(document.createTextNode("Product Information"));
updateHeading.setAttribute("align",'center');

var productId = document.createElement("input");
productId.setAttribute("type","number");
productId.setAttribute("id",'productId');
productId.setAttribute("placeholder",'Enter Product Id');
productId.setAttribute("required","true");

var productName = document.createElement("input");
productName.setAttribute("type","text");
productName.setAttribute("id",'name');
productName.setAttribute("pattern",'[A-Za-z]+');
productName.setAttribute("autofocus","on");
productName.setAttribute("placeholder",'Enter Product Name');
productName.setAttribute("required","true");

var desc =  document.createElement("input");
desc.setAttribute("type","text");
desc.setAttribute("id",'description');
desc.setAttribute("placeholder",'Enter Product Description');
desc.setAttribute("required","true");

var productPrice = document.createElement("input");
productPrice.setAttribute("type","number");
productPrice.setAttribute("id",'price');
productPrice.setAttribute("placeholder",'Enter Product Price');
productPrice.setAttribute("required",'true');

var rating = document.createElement("select");

var opt1 = document.createElement('option');
opt1.appendChild( document.createTextNode('excellent') );
opt1.value = 'excellent'; 
rating.appendChild(opt1);

var opt2 = document.createElement('option');
opt2.appendChild( document.createTextNode('good') );
opt2.value = 'good'; 
rating.appendChild(opt2);

var opt3 = document.createElement('option');
opt3.appendChild( document.createTextNode('average') );
opt3.value = 'good';

rating.appendChild(opt3);
var opt4 = document.createElement('option');
opt4.appendChild( document.createTextNode('poor') );
opt4.value = 'good'; 
rating.appendChild(opt4);

rating.setAttribute("id",'rating');
rating.setAttribute("required",'true');
var linebreak = document.createElement("br");

var addProductDbbtn = document.createElement("button");
var addProductDbTextNode = document.createTextNode("Add Product");
addProductDbbtn.appendChild(addProductDbTextNode);
addProductDbbtn.addEventListener('click',function()
{
   addProduct();
});

var displayProdbtn = document.createElement("button");
var displayProdbtnTextNode = document.createTextNode("ShowProducts");
displayProdbtn.appendChild(displayProdbtnTextNode);
displayProdbtn.addEventListener('click',function()
{
   displayProducts();
});

var prodIdDiv=  document.createElement("div");
prodIdDiv.appendChild(document.createTextNode("Product ID:"));

var nameDiv=  document.createElement("div");
nameDiv.appendChild(document.createTextNode("Product Name:"));

var descDiv=  document.createElement("div");
descDiv.appendChild(document.createTextNode("Product Description:"));

var priceDiv=  document.createElement("div");
priceDiv.appendChild(document.createTextNode("Price:"));

var ratingDiv=  document.createElement("div");
ratingDiv.appendChild(document.createTextNode("Rating:"));

var emptyDiv=  document.createElement("div");

var emptyDiv=  document.createElement("div");

var body = document.getElementsByTagName('body')[0];

document.body.appendChild(updateHeading);
// document.body.appendChild(idDiv);
// body.appendChild(id);
document.body.appendChild(prodIdDiv);
body.appendChild(productId)

document.body.appendChild(nameDiv);
body.appendChild(productName);

document.body.appendChild(descDiv);
body.appendChild(desc);
document.body.appendChild(priceDiv);
body.appendChild(productPrice);

document.body.appendChild(ratingDiv);
body.appendChild(rating);
body.appendChild(emptyDiv);
body.appendChild(linebreak);
body.appendChild(addProductDbbtn);
body.appendChild(displayProdbtn);
// body.appendChild(addProducttoCartbtn);


function addProduct(){
   var productId= document.getElementById("productId").value;
   var name=document.getElementById("name").value;
   var description =document.getElementById("description").value;
   var price=document.getElementById("price").value;
   var rating=document.getElementById("rating").value;
   
       if(!productId.length>0){
         alert("Please Enter productId");
          return;
        }
       if(!name.length>0){
            alert("Please Enter Name");
            return;
       }else if(!description.length>0){
             alert("Please Enter description");    
             return;       
        }else if(!price.length>0){
               alert("Please Enter price");   
               return;        
        }else if(!rating.length>0){
              alert("Please Enter rating"); 
              return;          
        }

   var obj={"productId":productId,"name":name,"description":description,"price":price,"rating":rating};  
   var httpReq;
   if(window.XMLHttpRequest){
       httpReq=new XMLHttpRequest();
   }else{
       httpReq=ActiveXObject("");
   }
   httpReq.onreadystatechange=function(){
       if(this.readyState===4 && this.status===201){
       alert("Product Successfuly Added... !!");
       console.log(this.response);
     //getData();
       }
   }
   httpReq.open("post","http://localhost:3000/products",true);
   //httpReq.send(obj);
   httpReq.setRequestHeader("Content-type","application/json");
   httpReq.send(JSON.stringify(obj));
}


function displayProducts(){  
   var tableEl = document.getElementsByTagName('table');
     if (tableEl[0] !== undefined) {
         tableEl[0].remove()
     } 
           
  var xmlhttp;
   if(window.XMLHttpRequest){
      xmlhttp=new XMLHttpRequest(); 
   }            
   xmlhttp.onreadystatechange = function() {   
   if (this.readyState === 4 && this.status === 200) {      
     console.log(this.response);
     displayTableData(this.response);
    }   
    }
    xmlhttp.open('get', 'http://localhost:3000/products', true); 
     xmlhttp.setRequestHeader("Content-type","application/json");
     xmlhttp.send(null);     
   // });
 } 
 
 //This method fetches all the user details
  function displayTableData(response)
  {
   var body = document.getElementsByTagName('body')[0];
   var table = document.createElement('table');
   //Delete the table if already exists
 
    var removeTable = document.getElementsByTagName('table');
     if (removeTable[0] !== undefined) {
         removeTable[0].remove()
      }
                
    /*table.setAttribute("border", "1");*/
    table.setAttribute("id", "users");
    
    
     var tbody = document.createElement('tbody');
     var thead = document.createElement('thead');
     var headTr = document.createElement('tr');
     
     var headTd1 = document.createElement('td');
     var headTd1text = document.createTextNode ("productId");
     headTd1.appendChild(headTd1text);
     
     var headTd2 = document.createElement('td');
     var headTd2text = document.createTextNode ("name");
     headTd2.appendChild(headTd2text);
     
     var headTd3 = document.createElement('td');
     var headTd3text = document.createTextNode("description");
     headTd3.appendChild(headTd3text);
 
     var headTd4 = document.createElement('td');
     var headTd4text = document.createTextNode ("price");
     headTd4.appendChild(headTd4text);
     
     var headTd5 = document.createElement('td');
     var headTd5text = document.createTextNode("rating");
     headTd5.appendChild(headTd5text);

     var headTd6 = document.createElement('td');
     var headTd6text = document.createTextNode("Action");
     headTd6.appendChild(headTd6text);
   
       
      // headTd7.appendChild(cartBtn);
     headTr.appendChild(headTd1);
     headTr.appendChild(headTd2);
     headTr.appendChild(headTd3);
     headTr.appendChild(headTd4);
     headTr.appendChild(headTd5);
     headTr.appendChild(headTd6);
     //headTr.appendChild(td7);
    
     
     thead.appendChild(headTr);
     var data = JSON.parse(response);
     var len = data.length;
     
     if(len > 0 ){
     for(var i=0; i<len;i++){
         var tbodyTr = document.createElement('tr');
 
         var td1 = document.createElement('td');
         var td1Text = document.createTextNode(data[i].productId);
         td1.appendChild(td1Text);
 
         var td2 = document.createElement('td');
         var td2Text = document.createTextNode(data[i].name);
         td2.appendChild(td2Text);
 
         var td3 = document.createElement('td');
         var td3Text = document.createTextNode(data[i].description);
         td3.appendChild(td3Text);
 
         var td4 = document.createElement('td');
         var td4Text = document.createTextNode(data[i].price);
         td4.appendChild(td4Text);
 
         var td5 = document.createElement('td');
         var td5Text = document.createTextNode(data[i].rating);
         td5.appendChild(td5Text);
 
         var td6=document.createElement('td');

         var updatebutton=document.createElement('button');
         var updatebuttontxt=document.createTextNode("addCart");
         updatebutton.addEventListener("click",function(){
             var data=this.parentElement.parentElement.cells;
             console.log("data: "+data[0].innerHTML+" "+data[1].innerHTML+"  "+data[2].innerHTML+" "+data[3].innerHTML+" "+data[4].innerHTML);
           
             var userId = localStorage.getItem("id");
            
            var obj = {productId : data[0].innerHTML, name : data[1].innerHTML , description : data[2].innerHTML , price:data[3].innerHTML , rating:data[4].innerHTML}; 
           
            localStorage.setItem("products", JSON.stringify(obj));
            console.log("Successfully added item to cart")
           window.location.href="cartDetails.html";        	
         })
         
         updatebutton.appendChild(updatebuttontxt);
         td6.appendChild(updatebutton);

         tbodyTr.appendChild(td1);
         tbodyTr.appendChild(td2);
         tbodyTr.appendChild(td3);
         tbodyTr.appendChild(td4);
         tbodyTr.appendChild(td5);    
         tbodyTr.appendChild(td6);           
         tbody.appendChild(tbodyTr); 
     }
     }
     else{
         var data = document.createElement("h4");
         var noData = document.createTextNode("No data Found")
         data.appendChild(noData);
         tbody.appendChild(data);
     }
     
     table.appendChild(thead);
     table.appendChild(tbody);
     
     body.appendChild(table);
   }

   