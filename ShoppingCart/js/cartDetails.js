//addCart Details
function addCart(){ 
    
    var productId = document.getElementById("productId").value;
    var name=document.getElementById("name").value;
   // var description =document.getElementById("description").value;
    var price=document.getElementById("price").value;
    var userId=document.getElementById("userId").value;
    // var rating=document.getElementById("rating").value;
    var qty=document.getElementById("prodQty").value;
    var productPrice = parseInt(price)*parseInt(qty);
    var productpricedetails = productPrice+"";
    
    
         if(!productId.length>0){
               alert("Please Enter productId");
               return;
          }
        else if(!name.length>0){
             alert("Please Enter Name");
             return;
        }else if(!price.length>0){
                alert("Please Enter price");   
                return;        
         }
         else if(!userId.length>0){
          alert("Please Enter userId"); 
          return;          
        }
        else if(!qty.length>0){
          alert("Please Enter qty"); 
          return;          
        }
 
    var obj = {productId : productId, name : name , userId : userId , price:productpricedetails,prodQty:qty};
    console.log(obj);  
    var httpReq;
    if(window.XMLHttpRequest){
        httpReq=new XMLHttpRequest();
    }else{
        httpReq=ActiveXObject("");
    }
    httpReq.onreadystatechange=function(){
        if(this.readyState===4 && this.status===201){
        alert("Item Added to cart successfully... !!");
        console.log(this.response);
      //getData();
        }
    }
    httpReq.open("post","http://localhost:3000/carts",true);
    //httpReq.send(obj);
    httpReq.setRequestHeader("Content-type","application/json");
    httpReq.send(JSON.stringify(obj));
 }

 function getCartProduct(){   
  var data=JSON.parse(localStorage.getItem("products"));
  var cartUserId = localStorage.getItem("userId");
  console.log("data: "+data.productName);
  document.getElementById("productId").value=data.productId;
  document.getElementById("name").value=data.name;
  document.getElementById("userId").value=cartUserId;
  document.getElementById("price").value=data.price;  
}

function getCarts(){  
  var tableEl = document.getElementsByTagName('table');
    if (tableEl[0] !== undefined) {
        tableEl[0].remove()
    }
	
    var httpReq;
    if(window.XMLHttpRequest) {
        httpReq = new XMLHttpRequest();
    }
    else{
        httpReq = new ActiveXObject("")
    }
    httpReq.onreadystatechange = function() {

        if(this.readyState ===4 && this.status === 200){
            console.log(this.response);
            localStorage.setItem("carts",this.response);
            displaytableCart(this.response);
        }
    }
   
    httpReq.open('get', 'http://localhost:3000/carts?userId='+localStorage.getItem("userId"), true);
    httpReq.send();
  }

  function displaytableCart(response){
    	
	var body = document.getElementsByTagName('body')[0];
	var table = document.createElement('table');
	table.setAttribute("border", "1");
	
	
    var tbody = document.createElement('tbody');
    var thead = document.createElement('thead');
    var headTr = document.createElement('tr');
    
    var headTd1 = document.createElement('td');
    var headTd1text = document.createTextNode ("Id");
    headTd1.appendChild(headTd1text);
    
    var headTd2 = document.createElement('td');
    var headTd2text = document.createTextNode ("productId");
    headTd2.appendChild(headTd2text);
    
    var headTd3 = document.createElement('td');
    var headTd3text = document.createTextNode("name");
    headTd3.appendChild(headTd3text);

    var headTd4 = document.createElement('td');
    var headTd4text = document.createTextNode("userId");
    headTd4.appendChild(headTd4text);

    var headTd5 = document.createElement('td');
    var headTd5text = document.createTextNode("price");
    headTd5.appendChild(headTd5text);

    var headTd6 = document.createElement('td');
    var headTd6text = document.createTextNode ("prodQty");
    headTd6.appendChild(headTd6text);
    
    

    headTr.appendChild(headTd1);
    headTr.appendChild(headTd2);
    headTr.appendChild(headTd3);
    headTr.appendChild(headTd4);
    headTr.appendChild(headTd5);
    headTr.appendChild(headTd6);


    
    thead.appendChild(headTr);
    var data = JSON.parse(response);
    console.log(data);
    var len = data.length;
    
    if(len > 0 ){
    for(var i=0; i<len;i++){
        var tbodyTr = document.createElement('tr');

        var td1 = document.createElement('td');
        var td1Text = document.createTextNode(data[i].id);
        td1.appendChild(td1Text);

        var td2 = document.createElement('td');
        
        var td2Text = document.createTextNode(data[i].productId);
        td2.appendChild(td2Text);

        var td3 = document.createElement('td');
        var td3Text = document.createTextNode(data[i].name);
        td3.appendChild(td3Text);

        var td4 = document.createElement('td');
        var td4Text = document.createTextNode(data[i].userId);
        td4.appendChild(td4Text);

        var td5 = document.createElement('td');
        var td5Text = document.createTextNode(data[i].price);
        td5.appendChild(td5Text);

        var td6 = document.createElement('td');
        var td6Text = document.createTextNode(data[i].prodQty);
        td6.appendChild(td6Text);
        tbodyTr.appendChild(td1);
        tbodyTr.appendChild(td2);
        tbodyTr.appendChild(td3);
        tbodyTr.appendChild(td4);
        tbodyTr.appendChild(td5);
       
        tbodyTr.appendChild(td6);
        tbody.appendChild(tbodyTr); 
    }
    }
    else{
        var data = document.createElement("h4");
        var noData = document.createTextNode("No data Found")
        data.appendChild(noData);
        tbody.appendChild(data);
    }
    
    table.appendChild(thead);
    table.appendChild(tbody);    
    body.appendChild(table);
	
}

function addNewProductsToCart(){
  window.location.href="Product.html";
}


function removeCart(id){
  var xmlhttp;
  if(window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest();
  }
  xmlhttp.onreadystatechange= function(){
      if(this.status === 200 && this.readyState===4){
          alert("removed cart items Successfully");
      }
  }
  xmlhttp.open("DELETE", "http://localhost:3000/carts", true);
  xmlhttp.setRequestHeader('Content-type','application/json');
  xmlhttp.send(null);
  
}

function placeOrderData(){

  var cartlist = JSON.parse(localStorage.getItem("carts"));
  console.log("cartlist :=>"+cartlist.length);
  var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var dateTime = date+' '+time;
alert(dateTime);
 for (i in cartlist) {
     
      var orderObj = {
          productId : cartlist[i].productId, 
          name : cartlist[i].name , 
          userId :cartlist[i].userId,
          price:cartlist[i].price,
          orderDate : dateTime.toString()
      };
      
    placeOrder(orderObj);
  }// window.location.href="PlaceOrder.html";
}