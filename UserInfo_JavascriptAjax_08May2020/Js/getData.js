   
   
 function getData(){               
    console.log("In getData()");          
var userTable = document.getElementById('userTable');
if(userTable!=null && typeof(userTable)!=='undefined'){
    userTable.remove();
}
var httpReq;
if(window.XMLHttpRequest){
    httpReq = new XMLHttpRequest();
}else{
    httpReq = new ActiveXObject("");
}

httpReq.onreadystatechange=function(){       
    if(this.readyState===4 && this.status===200){   
        console.log(this.response);
        alert(this.response) ;    
        displayTableData(this.response);

       
    }
}

httpReq.open('get','http://localhost:3000/users',true);
httpReq.send();
}
  

function  displayTableData(response){    
var body = document.getElementsByTagName('body')[0];
var table = document.createElement('table');
table.setAttribute("id","userTable");
table.setAttribute("name","myTable");
var tbody = document.createElement('tbody');
var thead = document.createElement('thead');

var headTr = document.createElement('tr');
var headTd1 = document.createElement('td');
var headTd1Text = document.createTextNode('id');
headTd1.appendChild(headTd1Text);

var headTd2 = document.createElement('td');
var headTd2Text = document.createTextNode('userName');
headTd2.appendChild(headTd2Text);

var headTd3 = document.createElement('td');
var headTd3Text = document.createTextNode('userMobile');
headTd3.appendChild(headTd3Text);

var headTd4 = document.createElement('td');
var headTd4Text = document.createTextNode('userSapId');
headTd4.appendChild(headTd4Text);

var headTd5 = document.createElement('td');
var headTd5Text = document.createTextNode('userEmail');
headTd5.appendChild(headTd5Text);

var headTd6 = document.createElement('td');
var headTd6Text = document.createTextNode('Action');
headTd6.appendChild(headTd6Text);

headTr.appendChild(headTd1);
headTr.appendChild(headTd2);
headTr.appendChild(headTd3);
headTr.appendChild(headTd4);
headTr.appendChild(headTd5);
headTr.appendChild(headTd6);
var section = document.getElementsByTagName('section')[0];
section.appendChild(table);
var data = JSON.parse(response);
var datalen = data.length;
if(datalen>0){
console.log(datalen);
var removeTab = document.getElementById("dataTable");                   
var body = document.getElementsByTagName('body')[0];
for(var i=0; i<datalen; i++){
        alert(data[i].userName,data[i].userMobile,data[i].userSapId,data[i].userEmail,data[i].id);
        var tbodytr = document.createElement('tr');

        var tbodytrtd1 = document.createElement('td');
        var td1Text = document.createTextNode(data[i].id);
        tbodytrtd1.appendChild(td1Text);

        var tbodytrtd2 = document.createElement('td');
        var td2Text = document.createTextNode(data[i].userName);
        tbodytrtd2.appendChild(td2Text);

        var tbodytrtd3 = document.createElement('td');
        var td3Text = document.createTextNode(data[i].userMobile);
        tbodytrtd3.appendChild(td3Text);

        var tbodytrtd4 = document.createElement('td');
        var td4Text = document.createTextNode(data[i].userSapId);
        tbodytrtd4.appendChild(td4Text);

        var tbodytrtd5 = document.createElement('td');
        var td5Text = document.createTextNode(data[i].userEmail);
        tbodytrtd5.appendChild(td5Text);

        var tbodytrtd6 = document.createElement('td');
        //delete
        var deleteBtn=document.createElement("button");                
        var deleteBtnTxt=document.createTextNode("Delete");
        deleteBtn.addEventListener('click',function(){                
            deleteUser(this);
        })
        deleteBtn.appendChild(deleteBtnTxt);

        //update
        var updateBtn=document.createElement("button");
        var  updateBtnTxt=document.createTextNode("Update");
            updateBtn.addEventListener('click',function(){
            var data=this.parentElement.parentElement.cells;
            console.log(data);
          //  console.log("data: "+data[0].innerHTML+" "+data[1].innerHTML+"  "+data[2].innerHTML);
            var obj1 = {id:data[0].innerHTML,userName:data[1].innerHTML,userMobile:data[2].innerHTML,userSapId:data[3].innerHTML,userEmail:data[4].innerHTML};
            console.log("Object: "+obj1);
            localStorage.setItem("users", JSON.stringify(obj1));
            window.location.assign("update.html");                         
            getData();                                  
         })
       
        updateBtn.appendChild(updateBtnTxt);
        tbodytrtd6.appendChild(deleteBtn);
        tbodytrtd6.appendChild(updateBtn);

        tbodytr.appendChild(tbodytrtd1);
        tbodytr.appendChild(tbodytrtd2);
        tbodytr.appendChild(tbodytrtd3);
        tbodytr.appendChild(tbodytrtd4);
        tbodytr.appendChild(tbodytrtd5);
        tbodytr.appendChild(tbodytrtd6);
        tbody.appendChild(tbodytr);

    }

}
else{

    var data = document.createElement("h4");
    var noData = document.createTextNode("No data Found");
    data.appendChild(noData);
    tbody.appendChild(data);
}
thead.appendChild(headTr);
table.appendChild(thead);
table.appendChild(tbody);
}

function deleteUser(id)
{
var httpReq;

if(window.XMLHttpRequest)
{
    httpReq = new XMLHttpRequest
}
else{
    httpReq  = new ActiveXObject();
}

httpReq.onreadystatechange = function()
{
     if(this.readyState===4 && this.status===200)         {
             alert("User deleted successfully");
             getData();
     }
     
}

var data = id.parentElement.parentElement.cells;
console.log("data"+data);
     var idtodelete = data[0].innerHTML;

//  var  idtodelete = document.getElementById("idtodel").value;

httpReq.open('DELETE',"http://localhost:3000/users/"+idtodelete,true);
httpReq.send();
}
    