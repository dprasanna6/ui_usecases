
function addData(){    
  var section = document.getElementsByTagName('section')[0];
  var form = document.createElement('form');
  form.setAttribute("id", "userForm");
  //form.setAttribute("method", "post"); 
  section.appendChild(form);

  var nameLabel = document.createElement('label');
  nameLabel.innerHTML = "Name : ";
  form.appendChild(nameLabel);

  var nameInput = document.createElement('input'); 
  nameInput.setAttribute("type", "text");
  nameInput.setAttribute("name", "name");
  nameInput.setAttribute("id", "userName");
  nameInput.setAttribute("pattern","[A-Za-z]+");
  nameInput.setAttribute("placeholder","Enter Name");
  nameInput.setAttribute("autofocus","on");
  form.appendChild(nameInput);

  var nameBreak = document.createElement('br');
  form.appendChild(nameBreak);

  var mobileLabel = document.createElement('label');
  mobileLabel.innerHTML = "Mobile : ";
  form.appendChild(mobileLabel);

  var mobileInput = document.createElement('input'); 
  mobileInput.setAttribute("type", "number");
  mobileInput.setAttribute("name", "mobile");
  mobileInput.setAttribute("id", "userMobile");
  mobileInput.setAttribute("min", "1");
  mobileInput.setAttribute("placeholder","Enter Mobile");
  form.appendChild(mobileInput);

  var mobileBreak = document.createElement('br');
  form.appendChild(mobileBreak);

  var sapLabel = document.createElement('label');
  sapLabel.innerHTML = "Sap Id : ";
  form.appendChild(sapLabel);

  var sapInput = document.createElement('input'); 
  sapInput.setAttribute("type", "number");
  sapInput.setAttribute("name", "sap");
  sapInput.setAttribute("id", "userSapId");
  sapInput.setAttribute("min", "1");
  sapInput.setAttribute("placeholder","Enter Sap Id");
  form.appendChild(sapInput);

  var sapIdBreak = document.createElement('br');
  form.appendChild(sapIdBreak);

  var emailLabel = document.createElement('label');
  emailLabel.innerHTML = "Email : ";
  form.appendChild(emailLabel);

  var emailInput = document.createElement('input'); 
  emailInput.setAttribute("type", "text");
  emailInput.setAttribute("name", "email");
  emailInput.setAttribute("id", "userEmail");
  emailInput.setAttribute("pattern","[a-z0-9._]+@[a-z]+\.[a-z]{3}$");
  emailInput.setAttribute("placeholder","Enter email");
  form.appendChild(emailInput);

  var emailBreak = document.createElement('br');
  form.appendChild(emailBreak);

  var submit = document.createElement('input');
  submit.setAttribute("type", "submit");
  submit.setAttribute("name", "submit");
  submit.setAttribute("value", "Submit");
  submit.setAttribute("align","center");
  form.appendChild(submit);
  form.addEventListener('submit', (event) => {
    event.preventDefault();
    var name = document.getElementById("userName").value;
    var mobile = document.getElementById("userMobile").value;
    var sapId = document.getElementById("userSapId").value;
    var email = document.getElementById("userEmail").value;
   
  var obj = {userName:name,userMobile:mobile,userSapId:sapId,userEmail:email};
   console.log(obj);
  //  var data=JSON.parse(obj); 
    // alert(data);
 var xmlhttp;
if(window.XMLHttpRequest){
  xmlhttp=new XMLHttpRequest(); 
}
              
xmlhttp.onreadystatechange = function() {   
  if (this.readyState == 4 && this.status == 201) {  
    alert("Added Successfully");  
  }              
};
 xmlhttp.open("post", "http://localhost:3000/users ",true);   
 xmlhttp.setRequestHeader("Content-type","application/json");
 xmlhttp.send(JSON.stringify(obj));
   
});
  
   
}




