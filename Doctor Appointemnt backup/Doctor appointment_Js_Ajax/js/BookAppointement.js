
var httpRequest;
if (window.XMLHttpRequest) {
    httpRequest = new XMLHttpRequest()
} else {
    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
httpRequest.onreadystatechange = function () {
    if (this.readyState === 4 && this.status == 200) {

        var tableEl = document.getElementsByTagName('table');
        if (tableEl[0] !== undefined) {
            tableEl[0].remove()
        }
var body = document.getElementsByTagName('body')[0];
var table = document.createElement('table');
table.setAttribute("id", "tab01");

//slots avaible for patients
var tbody = document.createElement('tbody');
var thead = document.createElement('thead');
var headTr = document.createElement('tr');


var headTd1 = document.createElement('td');
var headTd1text = document.createTextNode ("Id");
headTd1.appendChild(headTd1text);

var headTd2 = document.createElement('td');
var headTd2text = document.createTextNode ("Doctor Mail ID");
headTd2.appendChild(headTd2text);

var headTd3 = document.createElement('td');
var headTd3text = document.createTextNode("starttime");
headTd3.appendChild(headTd3text);

var headTd4 = document.createElement('td');
var headTd4text = document.createTextNode("endtime");
headTd4.appendChild(headTd4text);

var headTd5 = document.createElement('td');
var headTd5text = document.createTextNode("Specialize");
headTd5.appendChild(headTd5text);


var headTd6 = document.createElement('td');
var headTd6text = document.createTextNode ("Action");
headTd6.appendChild(headTd6text);

headTr.appendChild(headTd1);
headTr.appendChild(headTd2);
headTr.appendChild(headTd3);
headTr.appendChild(headTd4);
headTr.appendChild(headTd5);
headTr.appendChild(headTd6);

thead.appendChild(headTr);

var data = JSON.parse(this.response);
var len = data.length;

if(len > 0 ){
for(var i=0; i<len;i++){
    var tbodyTr = document.createElement('tr');

    var td1 = document.createElement('td');
    var td1Text = document.createTextNode(data[i].id);
    td1.appendChild(td1Text);

    var td2 = document.createElement('td');
    var td2Text = document.createTextNode(data[i].docEmail);
    td2.appendChild(td2Text);

    var td3 = document.createElement('td');
    var td3Text = document.createTextNode(data[i].starttime);
    td3.appendChild(td3Text);
    
    var td4 = document.createElement('td');
    var td4Text = document.createTextNode(data[i].endtime);
    td4.appendChild(td4Text);

    var td5 = document.createElement('td');
    var td5Text = document.createTextNode(data[i].specialize);
    td5.appendChild(td5Text);
    //BookAppointment
    var td6=document.createElement('td');
    var bt1=document.createElement('button');
    bt1.setAttribute("class", "ubutton");
    var bt1txt=document.createTextNode("Booking Slot");
    bt1.addEventListener("click",function(){    
      var useremail=JSON.parse(JSON.stringify(sessionStorage.getItem("email")));
      var data=this.parentElement.parentElement.cells;
      console.log("data: "+data[0].innerHTML+" "+data[1].innerHTML+"  "+data[2].innerHTML+" "+data[3].innerHTML+" "+data[4].innerHTML);
      var obj={docEmail:data[1].innerHTML ,starttime:data[2].innerHTML,endtime: data[3].innerHTML,specialize: data[4].innerHTML,email: useremail };
      console.log("Object: "+obj);
       var httpReq;
          if(window.XMLHttpRequest) {
              httpReq = new XMLHttpRequest();
          }
          else{
              httpReq = new ActiveXObject("");
          }
          httpReq.onreadystatechange = function() {
              if(this.readyState ===4 && this.status === 201){ 
                  console.log("response: "+this.response);
                  alert("Your Appointment placed Successfully! ");
                 // window.location.assign("PatientAppointments.html");                  
              }
          }
          httpReq.open('post', 'http://localhost:3000/appointment', true);
          httpReq.setRequestHeader("Content-type","application/json");
          httpReq.send(JSON.stringify(obj));
      //window.location.assign("updateuser.html");
    })
    
    bt1.appendChild(bt1txt);
    td6.appendChild(bt1);
    tbodyTr.appendChild(td1);
    tbodyTr.appendChild(td2);
    tbodyTr.appendChild(td3);
    tbodyTr.appendChild(td4);
    tbodyTr.appendChild(td5);
    tbodyTr.appendChild(td6);
    tbody.appendChild(tbodyTr); 
}
}
else{
    var data = document.createElement("h4");
    var noData = document.createTextNode("No data Found")
    data.appendChild(noData);
    tbody.appendChild(data);
}
table.appendChild(thead);
table.appendChild(tbody);
body.appendChild(table);
}
}
httpRequest.open("get", "http://localhost:3000/slots", true);
httpRequest.send();
