var updateHeading = document.createElement("h2");
updateHeading.appendChild(document.createTextNode("Add Doctor Details for Available slots"));
updateHeading.setAttribute("align", 'center');

var doctorMailId = document.createElement("input");
doctorMailId.setAttribute("type", "text");
doctorMailId.setAttribute("id", 'docEmail');
doctorMailId.setAttribute("placeholder", 'Enter doctor name');
doctorMailId.setAttribute("required", "true");

var specialize = document.createElement("select");

var opt1 = document.createElement('option');
opt1.appendChild(document.createTextNode('General physician'));
opt1.value = 'General physician';
specialize.appendChild(opt1);

var opt2 = document.createElement('option');
opt2.appendChild(document.createTextNode('Dermatoloist'));
opt2.value = 'Dermatoloist';
specialize.appendChild(opt2);

var opt3 = document.createElement('option');
opt3.appendChild(document.createTextNode('Homoepath'));
opt3.value = 'Homoepath';
specialize.appendChild(opt3);

var opt4 = document.createElement('option');
opt4.appendChild(document.createTextNode('Ayurvada'));
opt4.value = 'Ayurvada';
specialize.appendChild(opt4);

specialize.setAttribute("id", 'specialize');
specialize.setAttribute("required", 'true');
//starttime field
var starttime = document.createElement("input");
starttime.setAttribute("type", "datetime-local");
starttime.setAttribute("id", 'starttime');
starttime.setAttribute("placeholder", 'Start time');
starttime.setAttribute("required", "true");

var endtime = document.createElement("input");
endtime.setAttribute("type", "datetime-local");
endtime.setAttribute("id", 'endtime');
endtime.setAttribute("placeholder", 'Enter end time');
endtime.setAttribute("required", "true");

var linebreak = document.createElement("br");

var doctormailDiv = document.createElement("div");
doctormailDiv.appendChild(document.createTextNode("Doctor Email Id"));

var specializeDiv = document.createElement("div");
specializeDiv.appendChild(document.createTextNode("Specialization:"));

var startTimeDiv = document.createElement("div");
startTimeDiv.appendChild(document.createTextNode("Start Time:"));

var endTimeDiv = document.createElement("div");
endTimeDiv.appendChild(document.createTextNode("End Time:"));

var emptyDiv = document.createElement("div");

var emptyDiv = document.createElement("div");

var body = document.getElementsByTagName('body')[0];

document.body.appendChild(updateHeading);

document.body.appendChild(doctormailDiv);
body.appendChild(doctorMailId)

document.body.appendChild(specializeDiv);
body.appendChild(specialize);

document.body.appendChild(startTimeDiv);
body.appendChild(starttime);

document.body.appendChild(endTimeDiv);
body.appendChild(endtime);


body.appendChild(emptyDiv);
body.appendChild(linebreak);
//Adding add slot
var addProductDbbtn = document.createElement("button");
var addProductDbTextNode = document.createTextNode("Add Slot");
addProductDbbtn.appendChild(addProductDbTextNode);
addProductDbbtn.addEventListener('click', function () {
    addSlot();
});

body.appendChild(addProductDbbtn);

//doctor adding slot details
function addSlot() {
    var email = document.getElementById("docEmail").value;
    var specialize = document.getElementById("specialize").value;
    var starttime = document.getElementById("starttime").value;
    var endtime = document.getElementById("endtime").value;

    if (!email.length > 0) {
        alert("Please Enter emailId");
        return;
    }
    if (!specialize.length > 0) {
        alert("Please Enter Specialization");
        return;
    } else if (!starttime.length > 0) {
        alert("Please Enter starttime");
        return;
    } else if (!endtime.length > 0) {
        alert("Please Enter endTime");
        return;
    }

    var obj = { "email": docEmail, "specialize": specialize, "starttime": starttime, "endtime": endtime };
    var httpReq;
    if (window.XMLHttpRequest) {
        httpReq = new XMLHttpRequest();
    } else {
        httpReq = ActiveXObject("");
    }
    httpReq.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 201) {
            alert("Slot successfully Added... !!");
            console.log(this.response);
            //getData();
        }
    }
    httpReq.open("post", "http://localhost:3000/slots", true);
    //httpReq.send(obj);
    httpReq.setRequestHeader("Content-type", "application/json");
    httpReq.send(JSON.stringify(obj));
}


